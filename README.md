# Restaurant

Програмное обеспечение для менеджмента заказов в ресторане

## Setup

Для использования необходимы:

+ [Python 3](https://www.python.org/downloads/)
+ Virtualenv (`pip3 install virtualenv`)

Чтобы установить запустите:

1. `virtualenv.exe venv`

2. `venv\Scripts\pip3.exe install -r requirements.txt` (Windows)

Django manage.py: `venv\Scripts\python.exe manage.py`

Чтобы запустить сервер:

`venv\Scripts\python.exe manage.py runserver`

[django-admin and manage.py docs](https://docs.djangoproject.com/en/dev/ref/django-admin/)
