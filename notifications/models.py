from django.db import models
from django.contrib.auth import get_user_model


class Notification(models.Model):
    to = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    text = models.TextField()
