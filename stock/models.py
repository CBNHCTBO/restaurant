from django.db import models


def validate_not_negative(number):
    return number >= 0


class StockItem(models.Model):
    name = models.CharField(max_length=200)
    number_in_stock = models.IntegerField(validators=[validate_not_negative])
    expiration_date = models.DateField()
