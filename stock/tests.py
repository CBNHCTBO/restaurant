from django.test import TestCase

from stock.models import validate_not_negative


class ValidateNotNegativeTestCase(TestCase):
    def test_validate_not_negative(self):
        self.assertTrue(validate_not_negative(0))
        self.assertTrue(validate_not_negative(1))
        self.assertFalse(validate_not_negative(-1))
