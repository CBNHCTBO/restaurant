from django.db import models
import stock.models


class Dish(models.Model):
    name = models.CharField(max_length=200)
    required_stock = models.ManyToManyField(stock.models.StockItem)
    price = models.IntegerField()
