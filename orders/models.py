from django.db import models
from django.contrib.auth import get_user_model
import menu.models


class Order(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    dishes = models.ManyToManyField(menu.models.Dish)
